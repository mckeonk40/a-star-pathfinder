/*
 * A-start path finder
 */
package astar;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author mckeonk
 */
public class AStar extends JFrame{
    
    /** Content panel */
    private JPanel content = new JPanel();
    /** Control panel */
    private JPanel controlPanel = new JPanel();
    /** A grid of JPanels, basically the map for the pathfinder */
    public JPanel grid = new JPanel();
    /** The number of rows and columns in the grid */
    private final int GRID_SIZE = 25;
    /** the start and end squares */
    private Square start, end;
    /** Coordinates for obstacle squares */
    private int[][] obstacleCoords;
    /** ArrayList of all the square objects on the grid */
    private ArrayList<Square> squares;
//    private ArrayList<Square> squares = new ArrayList();
    /** The number of obstacles placed on the grid */
    private int numOfObstacles = 200;
    
    /** 
     * Start point 
     * @param args
     */
    public static void main(String[] args) {
        new AStar();
    }
    
    /** 
     * Constructor, initiates the frame and grid
     * Calls the findPath method
     */
    public AStar(){
        setLayout(new FlowLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        content.setLayout(new GridLayout(0,2));
        content.add(grid);
        content.add(controlPanel);
        
        setupControlPanel();
        
        add(content);
        initGrid();
        pack();
        setLocationRelativeTo(null);
        show();
    }
    
    /**
     * Set up the user controls in the controlPanel
     */
    private void setupControlPanel(){
        controlPanel.setLayout(new GridLayout(8,0));
        
        JLabel startCoordLabel = new JLabel("Enter the start point coordinates");
        JLabel endCoordLabel = new JLabel("Enter the end point coordinates");
        JLabel obstacleLabel = new JLabel("Enter the number of obstacles");
        
        JTextArea startRowInput = new JTextArea("0");
        JTextArea startColInput = new JTextArea("0");
        JTextArea endRowInput = new JTextArea("1");
        JTextArea endColInput = new JTextArea("1");
        JTextArea obstacleInput = new JTextArea("100");
        
        JButton applyBtn = new JButton("Apply grid");
        applyBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int obs;
                int[] startCoord = new int[2];
                int[] endCoord = new int[2];
                String msg = ""; 
                
                try {
                    msg = "number of obstacles must be between 0 and " + (GRID_SIZE*GRID_SIZE - 2);
                    obs = Integer.parseInt(obstacleInput.getText());
                    msg = "coordinates must be between 0 and " + GRID_SIZE;
                    startCoord[0] = Integer.parseInt(startRowInput.getText());
                    startCoord[1] = Integer.parseInt(startColInput.getText());
                    endCoord[0] = Integer.parseInt(endRowInput.getText());
                    endCoord[1] = Integer.parseInt(endColInput.getText());
                    
                    if ((startCoord[0] > GRID_SIZE - 1 || startCoord[0] < 0) ||
                            (startCoord[1] > GRID_SIZE - 1 || startCoord[1] < 0) || 
                            (endCoord[0] > GRID_SIZE - 1 || endCoord[0] < 0) || 
                            (endCoord[1] > GRID_SIZE - 1 || endCoord[1] < 0)){
                        throw new NumberFormatException();
                    }
                    
                    applyGrid(obs, startCoord, endCoord);
                    controlPanel.revalidate();
                    controlPanel.repaint();
                    
                } catch (java.lang.NumberFormatException ex){
                   JOptionPane.showMessageDialog(null, msg);
                }
            }
        });
        
        JButton randomBtn = new JButton("Generate random grid");
        JButton findPathBtn = new JButton("Find path");
        findPathBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                findPath();
            }
        });
                
        JPanel startPanel = new JPanel();
        startPanel.add(startCoordLabel);
        startPanel.add(startRowInput);
        startPanel.add(startColInput);
        
        JPanel endPanel = new JPanel();
        endPanel.add(endCoordLabel);
        endPanel.add(endRowInput);
        endPanel.add(endColInput);
        
        JPanel obstaclePanel = new JPanel();
        obstaclePanel.add(obstacleLabel);
        obstaclePanel.add(obstacleInput);
        
        JPanel btnPan1 = new JPanel();
        JPanel btnPan2 = new JPanel();

        btnPan1.add(applyBtn);
        btnPan1.add(randomBtn);
        
        btnPan2.add(findPathBtn);
        
        controlPanel.add(startPanel);
        controlPanel.add(endPanel);
        controlPanel.add(obstaclePanel);
        controlPanel.add(btnPan1);
        controlPanel.add(btnPan2);
    }
    
    /**
     * Creates random coordinates that will be obstacles on the grid
     */
    private void createRandomObstacles(){
        obstacleCoords = new int[numOfObstacles][];
        
        for (int i = 0; i < numOfObstacles; i++){
            int randRow = (int)(Math.random() * GRID_SIZE);
            int randCol = (int)(Math.random() * GRID_SIZE);
            int[] rc = {randRow, randCol};
            
            // check if obstacle already exists
            int count = 0;
            for (int j = 0; j < i; j++){
                if (Arrays.equals(rc, obstacleCoords[j])){
                    count++;
                }
            }
            // only allow each obstacle coordinate once
            if (count == 0){
                obstacleCoords[i] = rc;
            } else {
                i--;
            }
        }
    }
    
    private void applyGrid(int obs, int[] st, int[] en){
        numOfObstacles = obs;
        
        grid.removeAll();
        generateSquares();
        generateObstacles();
        
        for (Square s : squares){
            if (s.getRow() == st[0] && s.getCol() == st[1]){
                start = s;
                s.setStart(true);
                s.setObstacle(false);
                start.setBackground(Color.GREEN);
            } else if (s.getRow() == en[0] && s.getCol() == en[1]){
                end = s;
                s.setEnd(true);
                s.setObstacle(false);
                end.setBackground(Color.RED);
            }
        }
    }
    
    private void generateObstacles(){
        createRandomObstacles();
        
        for (Square s : squares){
            int[] coords = new int[2];
            coords[0] = s.getRow();
            coords[1] = s.getCol();
            
            for (int i = 0; i < numOfObstacles; i++){
                if (Arrays.equals(obstacleCoords[i], coords)){
                    s.setObstacle(true);
                    break;
                }
            }
        }
    }
    
    private void generateSquares(){
        squares = new ArrayList();
        
        grid.setLayout(new GridLayout(GRID_SIZE, GRID_SIZE));
        
        for (int i = 0; i < GRID_SIZE; i++){
            for (int j = 0; j < GRID_SIZE; j++){
                
                int[] coords = {i, j};
                Square square = new Square(i,j);

                grid.add(square);
                squares.add(square);
                
                // set neighbors for all squares
                for (Square s : squares){
                    s.setNeighbors(squares);
                }
            }        
        }
    }
    
    /**
     * Creates the grid and all its square objects
     * Sets the start and end squares
     */
    private void initGrid(){
        squares  = new ArrayList();
        
        grid.setLayout(new GridLayout(GRID_SIZE, GRID_SIZE));
        
        for (int i = 0; i < GRID_SIZE; i++){
            for (int j = 0; j < GRID_SIZE; j++){
                
                int[] coords = {i, j};
                Square square = new Square(i,j);                               
                
                grid.add(square);
                squares.add(square);
                
                // set neighbors for all squares
                for (Square s : squares){
                    s.setNeighbors(squares);
                }
            }        
        }
        
    }
    
    /** initiates pathfinding */
    public void findPath(){
        Path pathFinder = new Path(start, end);
        
        if (pathFinder.hasSolution()){
            ArrayList<Square> path = pathFinder.getPath();
            for (Square s : path){
                s.setBackground(Color.YELLOW);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No path was found.");
        }
        start.setBackground(Color.GREEN);
        end.setBackground(Color.RED);
    }
}
