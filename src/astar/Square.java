package astar;

import java.awt.*;
import java.util.*;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 *
 * @author mckeonk
 */
public class Square extends JPanel {
    
    // attributes used for path finder algorithm
    private int row;
    private int col;
    private float fScore = 0;
    private float gScore = 0;
    private boolean isEnd = false;
    private boolean isStart = false;
    private boolean isObstacle = false;
    private boolean isTarget = false;
    private float center[] = new float[2];;
    private ArrayList<Square> neighbors = new ArrayList<>();
    private Square previous;
    
    
    /**
     * Square constructor, initializes a Square object
     * @param r represents the row of the square
     * @param c represents the column of the square
    */
    public Square(int r, int c){
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setBorder(BorderFactory.createStrokeBorder(new BasicStroke(0.5f)));
        this.row = r;
        this.col = c;
    }
    
    /**
     * Returns this square's row
     * @return row
     */
    public int getRow() { return this.row; };
    
    /**
     * Returns this square's col
     * @return col
     */
    public int getCol() { return this.col; };
    
    /**
     * Sets the background color of this square to Black
     */
    public void setBackgroundBlack(){
        this.setBackground(Color.BLACK);
    }
    
    /**
     * Defines if this square is the target (end square in path)
     * @param bool true or false
     */
    public void setTarget(boolean bool){ 
        this.isTarget = bool; 
    }
    
    /**
     * @return true if this square is currently the target
     */
    public boolean isTarget(){ return this.isTarget; }
    
    
    /**
     * sets FScore (for path-finding)
     * The cost to get from this square to the end square;
     * FScore = GScore + HScore, where HScore is a heuristic estimate
     * of the distance to the end square
     */
    public void setFScore(float f){
        this.fScore = f;
    }
    
    /**
     * @return The FScore of this square
     */
    public float getFScore(){ return this.fScore; }
    
    /**
     * sets GScore (for path-finding):
     * The distance from the start square to this square
     */
    public void setGScore(float g){
        this.gScore = g;
    }
    
    /**
     * @return The GScore of this square
     */
    public float getGScore(){ return this.gScore; }
    
    /**
     * Defines this square as the end square (for path-finding)
     */
    public void setEnd(boolean bool){
        this.isEnd = bool;
        this.setBackground(Color.GREEN);
    }
    
    /**
     * @return true if this is the end square in the path, otherwise false
     */
    public boolean isEnd(){ return this.isEnd; }
    
    /**
     * Defines this square as the start square (for path-finding)
     */
    public void setStart(boolean bool){
        this.isStart = bool;
        this.setBackground(Color.GREEN);
    }
    
    /**
     * @return true if this is the start square in the path, otherwise false
     */
    public boolean isStart(){ return this.isStart; }
    
    /**
     * Defines this square as an obstacle (for path-finding)
     * An obstacle is a square that can't serve as a part of the path.
     * A square that has a number is automatically an obstacle.
     */
    public void setObstacle(boolean bool){
        this.isObstacle = bool;
        this.setBackground(Color.BLACK);
    }
    
    /**
     * @return true if this square is an obstacle (a number square), otherwise false
     */
    public boolean isObstacle(){ return this.isObstacle; }
    
    /**
     * Sets the center of each square to the x, y position of the 
     * square in the grid plus half the width and height
     */
    public void setCenter(float x, float y){
        center[0] = x;
        center[1] = y;
    }   
    
    /**
     * @return Returns the center coordinates of this square
     */
    public float[] getCenter(){ 
        return center; 
    }   
    
    /**
     * Adds a neighbor to the neighbors ArrayList
     * @param s the new Neighbor
     */
    public void setNeighbor(Square s){
        this.neighbors.add(s);
    }
    
    /**
     * Sets all the neighbors of this square and adds them to the neighbors ArrayList
     * Neighbors can only be directly above or next to the square, not diagonally
     * @param squares ArrayList that holds all the squares
     */
    public void setNeighbors(ArrayList<Square> squares){
        this.neighbors.clear();
        for (Square s : squares){
            if ((s.getRow() == this.getRow()) && ((s.getCol() <= this.getCol() + 1) && (s.getCol() >= this.getCol() - 1))
                    || ((s.getCol() == this.getCol()) && ((s.getRow() <= this.getRow() + 1) && (s.getRow() >= this.getRow() - 1)))){
                this.neighbors.add(s);
            }
        }
    }
    
    /**
     * Sets the neighbors for this square (for path-finding).
     * Since diagonal movement is not allowed, the maximum number of
     * neighbors is 4: One to the right, left, top and bottom.
     */
    public ArrayList<Square> getNeighbors() {
        return this.neighbors;
    }
    
    /**
     * Sets the previous square (for path-finding).
     * When creating the final path to get from one square to another,
     * the path is retraced from end (if the end square can't be reached, the last
     * reachable square) to start by getting the previous (the square it came from)
     * square to this square.
     */
    public void setPrevious(Square p){
        this.previous = p;
    }
    
    /**
     * @return In pathfinding, this returns the last square in the path before this square
     */
    public Square getPrevious(){ return this.previous; }
    
}
