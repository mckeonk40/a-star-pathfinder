package astar;

import java.awt.Color;
import java.util.*;

/**
 *
 * @author mckeonk
 */
public class Path {
    
    public ArrayList<Square> closedSet = new ArrayList<>();
    public ArrayList<Square> openSet = new ArrayList<>();
    public ArrayList<Square> path = new ArrayList<>();
    
    private final Square endSquare;
    private final Square startSquare;
    
    private Square last;
    
    private boolean hasSolution = false;
    
    
    /**
     * Initializes a new Path object.
     * getNextSquare() will be called from here until it returns either 1 or -1
     * @param start The start Square
     * @param end The end Square
     */
    public Path(Square start, Square end){
        this.endSquare = end;
        this.startSquare = start; 
        
        // add startSquare to openSet as a starting point
        openSet.add(startSquare);
        
        // set startSquare gScore = 0
        startSquare.setGScore(0);
        // set startSquare f score (heuristic value)
        startSquare.setFScore(calcHScore(startSquare));
        
        int i = 0;
        while (i == 0){
            i = getNextSquare();
        }
        
        if (i == 1) {
            setSolution(true);
            
        } else if (i == -1) {
            setSolution(false);
        }
    }
    
    /**
     * Finds the best next square on the way from start to end
     * @return 0 to keep going, -1 if there is no solution, 1 if the end was reached
     */
    public int getNextSquare(){
        while (openSet.size() > 0) {
            Square current = openSet.get(0); 
            for (int i = 1; i < openSet.size(); i++){
                if (openSet.get(i).getFScore() < current.getFScore()){
                    current = openSet.get(i);
                }
            }
            // check if new current square is end square
            if (current == endSquare){
                last = current;
                return 1;
            }
            
            openSet.remove(current);
            closedSet.add(current);
            
            // add all neighbors to openSet, if they're not already in closedSet
            for (Square n : current.getNeighbors()){
                if (!closedSet.contains(n)){
                    // for non-diagonal movement: the distance from start to a neighbor 
                    // is the distance from start to curernt (gScore of current) plus the
                    // width of the square (distance between current and neighbor = width)
                    float tempG = current.getGScore() + (float) n.getWidth();
                    
                    // if neighbor was evaluated before, compare gScores 
                    // (check if there was a better way to get to this neighbor)
                    if (!openSet.contains(n) && !n.isObstacle()){
                        openSet.add(n);
                    } else {
                        if (tempG > n.getGScore()){
                            continue;
                        }
                    }
                    
                    n.setGScore(tempG);
                    n.setFScore(n.getGScore() + calcHScore(n));
                    n.setPrevious(current);
                    last = n;
                }
            }
            return 0;
        }
        return -1;
    }
    
    /**
     * Calculates the direct distance from the center of the square s to the end square
     * uses Pythagorean triangle to calculate diagonal distance
     * @param s The square this HScore belongs to
     * @return The direct distance from s to end
     */
    public float calcHScore(Square s){
        double xDist = endSquare.getCenter()[0] - s.getCenter()[0];
        double yDist = endSquare.getCenter()[1] - s.getCenter()[1];
        
        float distance = (float) Math.sqrt(Math.pow(xDist, 2.0) + Math.pow(yDist, 2.0));
        
        return distance;
    }

    // true if there is a path, otherwise returns false 
    private void setSolution(boolean bool){
        hasSolution = bool;
    }
    
    /**
     * @return true if there is a path, otherwise returns false 
     */
    public boolean hasSolution(){ return hasSolution; }
    
    /**
     * Retraces and returns the path. If no path was found, the path array lists 
     * all the squares that were found before the algorithm returned -1 (no path)
     * @return path The path as an ArrayList of Square objects
     */
    public ArrayList<Square> getPath(){
        path.add(last);
        Square next = last.getPrevious();

        try {
            while(next != startSquare){
                path.add(next);
                next = next.getPrevious();
            }
        } catch (NullPointerException e){
        }
        return path;
    }
    
}
